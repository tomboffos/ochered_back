import graphene
from graphene_django import DjangoObjectType
from graphene_django.filter.utils import get_filtering_args_from_filterset
from graphene_django_optimizer import query as optimizer_query
from ochered.mutations import CustomSerializerMutation
from .serializers import CategorySerializer, CategoryDeleteSerializer
from .models import Category
from .filters import CategoryFilter


class CategoryType(DjangoObjectType):
    catalog_id = graphene.String()

    class Meta:
        model = Category


class CategoryQuery(graphene.ObjectType):
    category_list = graphene.List(CategoryType, **get_filtering_args_from_filterset(CategoryFilter, ''))
    category_detail = graphene.Field(CategoryType, id=graphene.ID(required=True))

    print(category_list)
    def resolve_category_list(self, info, **kwargs):
        queryset = Category.objects.perm_query(info.context.user)
        filtered = CategoryFilter(kwargs, queryset=queryset).qs
        return optimizer_query(filtered, info)

    def resolve_category_detail(self, info, **kwargs):
        queryset = Category.objects.perm_query(info.context.user)
        return optimizer_query(queryset, info).get(pk=kwargs['id'])


class CategorySave(CustomSerializerMutation):
    class Meta:
        serializer_class = CategorySerializer
        obj_type = CategoryType


class CategoryDelete(CustomSerializerMutation):
    class Meta:
        serializer_class = CategoryDeleteSerializer
        obj_type = CategoryType


class CategoryMutation(object):
    category_save = CategorySave.Field()
    category_delete = CategoryDelete.Field()