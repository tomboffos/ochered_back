from rest_framework import serializers
from ochered.serializers import CustomModelSerializer
from .models import Category


class CategorySerializer(CustomModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class CategoryDeleteSerializer(CustomModelSerializer):
    id = serializers.CharField()

    class Meta:
        model = Category
        fields = ('id',)

    def update(self, instance, validated_data):
        return instance.delete()
