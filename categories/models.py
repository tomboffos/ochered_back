from django.db import models
from ochered.models import TimestampedModel
from catalogs.models import Catalog
from files.models import File
from .querysets import CategoryQuerySet


class Category(TimestampedModel):
    catalog = models.ForeignKey(Catalog, related_name='categories', on_delete=models.CASCADE)
    cover = models.ForeignKey(File, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=100)
    companies_count = models.PositiveIntegerField(default=0)

    objects = CategoryQuerySet.as_manager()
