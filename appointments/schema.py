import graphene
from graphene_django import DjangoObjectType
from graphene_django.filter.utils import get_filtering_args_from_filterset
from ochered.mutations import CustomSerializerMutation
from graphene_django_optimizer import query as optimizer_query
from .models import Appointment
from .filters import AppointmentFilter
from .serializers import AppointmentSerializer, AppointmentCloseSerializer, AppointmentScoreSerializer, AppointmentReplySerializer, AppointmentProcessSerializer, AppointmentDeleteSerializer


class AppointmentType(DjangoObjectType):
    class Meta:
        model = Appointment


class AppointmentQuery(graphene.ObjectType):
    appointment_list = graphene.List(AppointmentType, **get_filtering_args_from_filterset(AppointmentFilter, ''))
    appointment_detail = graphene.Field(AppointmentType, id=graphene.ID(required=True))

    def resolve_appointment_list(self, info, **kwargs):
        queryset = Appointment.objects.perm_query(info.context.user)
        filtered = AppointmentFilter(kwargs, queryset=queryset).qs
        return optimizer_query(filtered, info)

    def resolve_appointment_detail(self, info, **kwargs):
        queryset = Appointment.objects.perm_query(info.context.user)
        return optimizer_query(queryset, info).get(pk=kwargs['id'])


class AppointmentSave(CustomSerializerMutation):
    class Meta:
        serializer_class = AppointmentSerializer
        obj_type = AppointmentType


class AppointmentProcess(CustomSerializerMutation):
    class Meta:
        serializer_class = AppointmentProcessSerializer
        obj_type = AppointmentType


class AppointmentClose(CustomSerializerMutation):
    class Meta:
        serializer_class = AppointmentCloseSerializer
        obj_type = AppointmentType


class AppointmentScore(CustomSerializerMutation):
    class Meta:
        serializer_class = AppointmentScoreSerializer
        obj_type = AppointmentType


class AppointmentReply(CustomSerializerMutation):
    class Meta:
        serializer_class = AppointmentReplySerializer
        obj_type = AppointmentType


class AppointmentDelete(CustomSerializerMutation):
    class Meta:
        serializer_class = AppointmentDeleteSerializer
        obj_type = AppointmentType


class AppointmentMutation(object):
    appointment_save = AppointmentSave.Field()
    appointment_process = AppointmentProcess.Field()
    appointment_close = AppointmentClose.Field()
    appointment_score = AppointmentScore.Field()
    appointment_reply = AppointmentReply.Field()
    appointment_delete = AppointmentDelete.Field()
