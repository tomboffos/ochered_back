import datetime
from django.utils import timezone
from django.db import models
from ochered.models import TimestampedModel
from clients.models import Client
from companies.models import Company
from services.models import Service
from masters.models import Master
from positions.models import Position
from .querysets import AppointmentQuerySet
from .constants import STATUSES, STATUS_WAITING, STATUS_PROCESSING, STATUS_FINISHED


class AppointmentManager(models.Manager):
    def create(self, **kwargs):
        number = None
        status = STATUS_WAITING
        master = kwargs.get('master', None)

        if not kwargs.get('start', None):
            last_number = super(AppointmentManager, self).filter(number__isnull=False).order_by('-created_at').first()
            if last_number:
                if last_number.created_at.date() < datetime.date.today():
                    number = 1
                else:
                    number = last_number.number + 1
            else:
                number = 1

            if master and not master.appointments.filter(status=STATUS_PROCESSING).exists():
                status = STATUS_PROCESSING
            else:
                master = Master.objects.filter(positions__service=kwargs['service']).exclude(
                    appointments__status=STATUS_PROCESSING).first()
                if master:
                    status = STATUS_PROCESSING

        return super(AppointmentManager, self).create(
            service=kwargs['service'],
            start=kwargs.get('start', None),
            end=kwargs['start'] + datetime.timedelta(minutes=kwargs['service'].duration) if kwargs.get('start',
                                                                                                       None) else None,
            client=kwargs['client'],
            number=number,
            master=master,
            status=status,
        )


class Appointment(TimestampedModel):
    service = models.ForeignKey(Service, null=True, related_name='appointments', on_delete=models.SET_NULL)
    master = models.ForeignKey(Master, null=True, related_name='appointments', on_delete=models.SET_NULL)
    company = models.ForeignKey(Company, null=True, related_name='appointments', on_delete=models.SET_NULL)

    # position = models.ForeignKey(Position, related_name='appointments', on_delete=models.CASCADE)
    client = models.ForeignKey(Client, related_name='appointments', on_delete=models.CASCADE)
    status = models.CharField(choices=STATUSES, default=STATUS_WAITING, max_length=10)
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)
    started_at = models.DateTimeField(null=True)
    finished_at = models.DateTimeField(null=True)
    number = models.PositiveSmallIntegerField(null=True)
    comment_client = models.TextField(blank=True)
    score = models.PositiveSmallIntegerField(default=0)
    comment_company = models.TextField(blank=True)

    objects = AppointmentManager.from_queryset(AppointmentQuerySet)()

    def close(self):
        self.status = STATUS_FINISHED
        self.finished_at = timezone.now()
        self.save()

        next_app = Appointment.objects.filter(
            service__in=Service.objects.filter(positions__master=self.master),
            status=STATUS_WAITING,
            start__isnull=False,
            start__lte=datetime.datetime.now(),
        ).order_by('start').first()

        if not next_app:
            next_app = Appointment.objects.filter(
                service__in=Service.objects.filter(positions__master=self.master),
                status=STATUS_WAITING,
                number__isnull=False,
            ).order_by('created_at').first()

        if next_app:
            next_app.status = STATUS_PROCESSING
            next_app.started_at = timezone.now()
            next_app.master = self.master
            next_app.save()

        return self

    def process(self, **kwargs):
        self.master = kwargs['master']
        self.save()
        return self

    def delete(self, using=None, keep_parents=False):
        deleted_id = self.id
        super(Appointment, self).delete(using, keep_parents)
        self.id = deleted_id
        return self
