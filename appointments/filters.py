import django_filters
from .models import Appointment


class AppointmentFilter(django_filters.FilterSet):
    class Meta:
        model = Appointment
        fields = {
            'client': ['exact'],
            'service': ['exact'],
            'status': ['exact'],
            'start': ['lte', 'gte'],
            'service__group__company': ['exact'],
            'end': ['lte', 'gte'],
            'master': ['exact'],
        }
