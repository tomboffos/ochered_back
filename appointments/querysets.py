from django.db.models import QuerySet
from users.models import User

class AppointmentQuerySet(QuerySet):
    def perm_query(self, user):
        if user.is_authenticated:
            if user.is_client:
                return self.filter(client=user.client).order_by('created_at')
            if user.is_company:
                return self.filter(service__group__company=user.company).order_by('-finished_at', 'start', 'created_at')

        return self.none()

    def perm_save(self, user):
        user = User.objects.get(pk=2)
        print(user.client)
        if user.is_authenticated and user.is_client:
            print(user.client)

            return self.filter(client=user.client)

        return self.filter(client=user.client)

    def perm_process(self, user):
        if user.is_authenticated and user.is_company:
            return self.filter(service__group__company=user.company)

        return None

    def perm_close(self, user):
        if user.is_authenticated:
            if user.is_client:
                return self.filter(client=user.client)
            if user.is_company:
                return self.filter(service__group__company=user.company)

        return None

    def perm_score(self, user):
        if user.is_authenticated and user.is_client:
            return self.filter(client=user.client)

        return None

    def perm_reply(self, user):
        if user.is_authenticated and user.is_company:
            return self.filter(service__group__company=user.company)

        return None

    def perm_delete(self, user):
        if user.is_authenticated and user.is_client:
            return self.filter(client=user.client)

        return None
