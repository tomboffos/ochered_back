from rest_framework import serializers
from ochered.serializers import CustomModelSerializer
from .models import Appointment
from .constants import STATUS_WAITING, STATUS_PROCESSING
from django.utils.timezone import now


class AppointmentSerializer(CustomModelSerializer):
    class Meta:
        model = Appointment
        fields = ('company', 'start')

    def create(self, validated_data):
        print(now())
        return Appointment.objects.create(**validated_data, client=self.context['request'].user.client)


class AppointmentProcessSerializer(CustomModelSerializer):
    id = serializers.CharField(required=True)

    class Meta:
        model = Appointment
        fields = ('id', 'master')
        extra_kwargs = {
            'master': {
                'required': True,
            }
        }

    def update(self, instance, validated_data):
        return instance.process(**validated_data)


class AppointmentCloseSerializer(CustomModelSerializer):
    id = serializers.CharField(required=True)

    class Meta:
        model = Appointment
        fields = ('id',)

    def validate(self, attrs):
        if self.instance.status != STATUS_PROCESSING:
            raise serializers.ValidationError('Status is not processing')
        return attrs

    def update(self, instance, validated_data):
        return instance.close()


class AppointmentScoreSerializer(CustomModelSerializer):
    id = serializers.CharField(required=True)

    class Meta:
        model = Appointment
        fields = ('id', 'comment_client', 'score')
        extra_kwargs = {
            'score': {
                'required': True,
            }
        }

    def validate_score(self, val):
        if not val:
            raise serializers.ValidationError('Score must be at least 1')
        return val

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class AppointmentReplySerializer(CustomModelSerializer):
    id = serializers.CharField(required=True)

    class Meta:
        model = Appointment
        fields = ('id', 'comment_company')
        extra_kwargs = {
            'comment_company': {
                'allow_blank': False,
            }
        }

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class AppointmentDeleteSerializer(CustomModelSerializer):
    id = serializers.CharField(required=True)

    class Meta:
        model = Appointment
        fields = ('id',)

    def validate(self, attrs):
        if self.instance.status != STATUS_WAITING:
            raise serializers.ValidationError('Status is not Waiting')
        return attrs

    def update(self, instance, validated_data):
        return instance.delete()
