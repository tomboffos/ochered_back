# Generated by Django 3.0.8 on 2020-08-12 02:58

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('appointments', '0007_auto_20200812_0254'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='comment_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
    ]
