# Generated by Django 3.0.8 on 2020-08-11 18:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('positions', '0002_auto_20200811_1830'),
        ('appointments', '0002_appointment_position'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='position',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='appointments', to='positions.Position'),
        ),
    ]
