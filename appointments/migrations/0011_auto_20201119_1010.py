# Generated by Django 3.0.8 on 2020-11-19 10:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appointments', '0010_appointment_score'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appointment',
            name='end',
        ),
        migrations.AddField(
            model_name='appointment',
            name='number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='start',
            field=models.DateTimeField(null=True),
        ),
    ]
