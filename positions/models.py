from django.db import models
from ochered.models import TimestampedModel
from services.models import Service
from masters.models import Master
from .querysets import PositionQuerySet


class Position(TimestampedModel):
    service = models.ForeignKey(Service, related_name='positions', on_delete=models.CASCADE)
    master = models.ForeignKey(Master, related_name='positions', on_delete=models.CASCADE)

    objects = PositionQuerySet.as_manager()
