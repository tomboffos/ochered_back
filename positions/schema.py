import graphene
from graphene_django import DjangoObjectType
from graphene_django.filter.utils import get_filtering_args_from_filterset
from ochered.mutations import CustomSerializerMutation
from graphene_django_optimizer import query as optimizer_query
from .models import Position
from .filters import PositionFilter
from .serializers import PositionSerializer, PositionDeleteSerializer


class PositionType(DjangoObjectType):
    class Meta:
        model = Position


class PositionQuery(graphene.ObjectType):
    position_list = graphene.List(PositionType, **get_filtering_args_from_filterset(PositionFilter, ''))
    position_detail = graphene.Field(PositionType, id=graphene.ID(required=True))

    def resolve_position_list(self, info, **kwargs):
        queryset = Position.objects.perm_query(info.context.user)
        filtered = PositionFilter(kwargs, queryset=queryset).qs
        return optimizer_query(filtered, info)

    def resolve_position_detail(self, info, **kwargs):
        queryset = Position.objects.perm_query(info.context.user)
        return optimizer_query(queryset, info).get(pk=kwargs['id'])


class PositionSave(CustomSerializerMutation):
    class Meta:
        serializer_class = PositionSerializer
        obj_type = PositionType


class PositionDelete(CustomSerializerMutation):
    class Meta:
        serializer_class = PositionDeleteSerializer
        obj_type = PositionType


class PositionMutation(object):
    position_save = PositionSave.Field()
    position_delete = PositionDelete.Field()
