from ochered.serializers import CustomModelSerializer
from .models import Position


class PositionSerializer(CustomModelSerializer):
    class Meta:
        model = Position
        fields = '__all__'


class PositionDeleteSerializer(CustomModelSerializer):
    class Meta:
        model = Position
        fields = ('id', )

    def update(self, instance, validated_data):
        return instance.delete()
