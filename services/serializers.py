from rest_framework import serializers
from ochered.serializers import CustomModelSerializer
from .models import Service


class ServiceSerializer(CustomModelSerializer):
    class Meta:
        model = Service
        fields = '__all__'

    def validate_duration(self, val):
        if val < 1:
            raise serializers.ValidationError('Min 1 minute.')
        return val


class ServiceDeleteSerializers(CustomModelSerializer):
    id = serializers.CharField(required=True)

    class Meta:
        model = Service
        fields = ('id',)

    def update(self, instance, validated_data):
        return instance.delete()
