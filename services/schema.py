import graphene
from graphene_django import DjangoObjectType
from graphene_django.filter.utils import get_filtering_args_from_filterset
from ochered.mutations import CustomSerializerMutation
from graphene_django_optimizer import query as optimizer_query
from .models import Service
from .filters import ServiceFilter
from .serializers import ServiceSerializer, ServiceDeleteSerializers


class ServiceType(DjangoObjectType):
    group_id = graphene.String()

    class Meta:
        model = Service


class ServiceQuery(graphene.ObjectType):
    service_list = graphene.List(ServiceType, **get_filtering_args_from_filterset(ServiceFilter, ''))
    service_detail = graphene.Field(ServiceType, id=graphene.ID(required=True))

    def resolve_service_list(self, info, **kwargs):
        queryset = Service.objects.perm_query(info.context.user)
        filtered = ServiceFilter(kwargs, queryset=queryset).qs
        return optimizer_query(filtered, info)

    def resolve_service_detail(self, info, **kwargs):
        queryset = Service.objects.perm_query(info.context.user)
        return optimizer_query(queryset, info).get(pk=kwargs['id'])


class ServiceSave(CustomSerializerMutation):
    class Meta:
        serializer_class = ServiceSerializer
        obj_type = ServiceType


class ServiceDelete(CustomSerializerMutation):
    class Meta:
        serializer_class = ServiceDeleteSerializers
        obj_type = ServiceType


class ServiceMutation(object):
    service_save = ServiceSave.Field()
    service_delete = ServiceDelete.Field()
