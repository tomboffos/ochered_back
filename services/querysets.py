from django.db.models import QuerySet


class ServiceQuerySet(QuerySet):
    def perm_query(self, user):
        if user.is_authenticated:
            if user.is_company:
                return self.filter(group__company=user.company)
            if user.is_client:
                return self

        return self.none()

    def perm_save(self, user):
        if user.is_authenticated and user.is_company:
            return self.none()

        return None

    def perm_delete(self, user):
        if user.is_authenticated and user.is_company:
            return self.filter(group__company=user.company)

        return None
