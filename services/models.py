from django.db import models
from ochered.models import TimestampedModel
from companies.models import Company
from groups.models import Group
from .querysets import ServiceQuerySet


class ServiceManager(models.Manager):
    def create(self, **kwargs):
        service = super(ServiceManager, self).create(**kwargs)

        # for master in service.company.masters.all():
        #     master.positions.create(service=service)

        return service


class Service(TimestampedModel):
    # company = models.ForeignKey(Company, related_name='services', on_delete=models.CASCADE)
    group = models.ForeignKey(Group, related_name='services', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    duration = models.PositiveSmallIntegerField()

    objects = ServiceManager.from_queryset(ServiceQuerySet)()

    class Meta(TimestampedModel.Meta):
        ordering = None
