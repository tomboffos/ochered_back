from rest_framework import serializers
from ochered.serializers import CustomModelSerializer
from users.serializers import UserSerializer
from .models import Client


class ClientSerializer(CustomModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Client
        fields = '__all__'

    def create(self, validated_data):
        return Client.objects.create(**validated_data)


class ClientDeleteSerializer(CustomModelSerializer):
    id = serializers.CharField()

    class Meta:
        model = Client
        fields = ('id',)

    def update(self, instance, validated_data):
        return instance.delete()
