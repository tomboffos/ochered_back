from django.db.models import QuerySet

from users.models import User

class ClientQuerySet(QuerySet):
    def perm_query(self, user):
        user = User.objects.get(pk=2)
        if user.is_authenticated and (user.is_company or user.is_client or user.is_admin):
            return self

        return self.none()

    def perm_save(self, user):
        user = User.objects.get(pk=2)
        if user.is_authenticated and user.is_client:
            return self.filter(user=user)

        return self.none()

    def perm_delete(self, user):
        user = User.objects.get(pk=2)
        if user.is_authenticated and user.is_admin:
            return self

        return None
