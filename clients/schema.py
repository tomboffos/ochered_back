import graphene
from graphene_django import DjangoObjectType
from graphene_django.filter.utils import get_filtering_args_from_filterset
from graphene_django_optimizer import query as optimizer_query
from ochered.mutations import CustomSerializerMutation
from .models import Client
from .serializers import ClientSerializer, ClientDeleteSerializer
from .filters import ClientFilter


class ClientType(DjangoObjectType):
    class Meta:
        model = Client


class ClientQuery(graphene.ObjectType):
    client_list = graphene.List(ClientType, **get_filtering_args_from_filterset(ClientFilter, ''))
    client_detail = graphene.Field(ClientType, id=graphene.ID(required=True))

    def resolve_client_list(self, info, **kwargs):
        queryset = Client.objects.perm_query(info.context.user)
        filtered = ClientFilter(kwargs, queryset=queryset).qs
        return optimizer_query(filtered, info)

    def resolve_client_detail(self, info, **kwargs):
        queryset = Client.objects.perm_query(info.context.user)
        return optimizer_query(queryset, info).get(pk=kwargs['id'])


class ClientSave(CustomSerializerMutation):
    class Meta:
        serializer_class = ClientSerializer
        obj_type = ClientType


class ClientDelete(CustomSerializerMutation):
    class Meta:
        serializer_class = ClientDeleteSerializer
        obj_type = ClientType


class ClientMutation(object):
    client_save = ClientSave.Field()
    client_delete = ClientDelete.Field()
