from django.db import models
from django.conf import settings
from ochered.models import TimestampedModel
from cities.models import City
from users.models import User
from users.constants import ROLE_CLIENT
from .querysets import ClientQuerySet


class ClientManager(models.Manager):
    def create(self, **kwargs):
        user_kwargs = kwargs.pop('user')

        user = User.objects.create(**user_kwargs, role=ROLE_CLIENT, phone=user_kwargs['username'])

        return super(ClientManager, self).create(**kwargs, user=user)


class Client(TimestampedModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    city = models.ForeignKey(City, default=1, on_delete=models.SET_DEFAULT)
    is_push = models.BooleanField(default=True)

    objects = ClientManager.from_queryset(ClientQuerySet)()

    def update(self, **kwargs):
        user_kwargs = kwargs.pop('user')
        self.user.update(**user_kwargs)
        return super(Client, self).update(**kwargs)

    def delete(self, using=None, keep_parents=False):
        user = self.user
        super(Client, self).delete(using, keep_parents)
        user.delete()
        return self
