from django.db.models import QuerySet


class LikeQuerySet(QuerySet):
    def perm_query(self, user):
        if user.is_authenticated and user.is_client:
            return self.filter(client=user.client)

        return self.none()

    def perm_save(self, user):
        if user.is_authenticated and user.is_client:
            return self.filter(client=user.client)

        return self.none()

    def perm_delete(self, user):
        if user.is_authenticated and user.is_client:
            return self.filter(client=user.client)

        return self.none()
