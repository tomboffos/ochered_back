from rest_framework import serializers
from ochered.serializers import CustomModelSerializer
from .models import Like


class LikeSerializer(CustomModelSerializer):
    id = None

    class Meta:
        model = Like
        exclude = ('client',)

    def create(self, validated_data):
        return Like.objects.create(**validated_data, client=self.context['request'].user.client)


class LikeDeleteSerializer(CustomModelSerializer):
    id = serializers.CharField()

    class Meta:
        model = Like
        fields = ('id',)

    def update(self, instance, validated_data):
        return instance.delete()
