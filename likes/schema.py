import graphene
from graphene_django import DjangoObjectType
from graphene_django.filter.utils import get_filtering_args_from_filterset
from graphene_django_optimizer import query as optimizer_query
from ochered.mutations import CustomSerializerMutation
from .models import Like
from .filters import LikeFilter
from .serializers import LikeSerializer, LikeDeleteSerializer


class LikeType(DjangoObjectType):
    class Meta:
        model = Like


class LikeQuery(graphene.ObjectType):
    like_list = graphene.List(LikeType, **get_filtering_args_from_filterset(LikeFilter, ''))

    def resolve_like_list(self, info, **kwargs):
        queryset = Like.objects.perm_query(info.context.user)
        filtered = LikeFilter(kwargs, queryset=queryset).qs
        return optimizer_query(filtered, info)


class LikeSave(CustomSerializerMutation):
    class Meta:
        serializer_class = LikeSerializer
        obj_type = LikeType


class LikeDelete(CustomSerializerMutation):
    class Meta:
        serializer_class = LikeDeleteSerializer
        obj_type = LikeType


class LikeMutation(object):
    like_save = LikeSave.Field()
    like_delete = LikeDelete.Field()
