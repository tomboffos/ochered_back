from django.db import models
from ochered.models import TimestampedModel
from clients.models import Client
from companies.models import Company
from .querysets import LikeQuerySet


class Like(TimestampedModel):
    client = models.ForeignKey(Client, related_name='likes', on_delete=models.CASCADE)
    company = models.ForeignKey(Company, related_name='likes', on_delete=models.CASCADE)

    objects = LikeQuerySet.as_manager()

    class Meta(TimestampedModel.Meta):
        unique_together = ['client', 'company']
