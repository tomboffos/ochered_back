from django.db import models
from django.conf import settings
from ochered.models import TimestampedModel
from categories.models import Category
from files.models import File
from cities.models import City
from .querysets import CompanyQuerySet
from users.models import User
from users.constants import ROLE_COMPANY


class CompanyManager(models.Manager):
    def create(self, **kwargs):
        user_kwargs = kwargs.pop('user')

        user = User.objects.create(**user_kwargs, role=ROLE_COMPANY, phone=user_kwargs['username'])

        return super(CompanyManager, self).create(**kwargs, user=user)


class Company(TimestampedModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, null=True, related_name='companies', on_delete=models.SET_NULL)
    city = models.ForeignKey(City, null=True, on_delete=models.SET_NULL)
    logo = models.ForeignKey(File, related_name='logo_companies', null=True, on_delete=models.SET_NULL)
    cover = models.ForeignKey(File, related_name='cover_companies', null=True, on_delete=models.SET_NULL)
    name = models.CharField(blank=True, max_length=100)
    start = models.TimeField(null=True)
    end = models.TimeField(null=True)
    scores_count = models.PositiveIntegerField(default=0)
    appointments_count = models.PositiveIntegerField(default=0)

    objects = CompanyManager.from_queryset(CompanyQuerySet)()

    def update(self, **kwargs):
        user_kwargs = kwargs.pop('user')
        self.user.update(**user_kwargs)

        if 'category' in kwargs and kwargs['category'] != self.category:
            if self.category:
                self.category.companies_count -= 1
                self.category.save()
            if kwargs['category']:
                kwargs['category'].companies_count += 1
                kwargs['category'].save()

        return super(Company, self).update(**kwargs)

    def delete(self, using=None, keep_parents=False):
        user = self.user
        for master in self.masters.all():
            master.delete()
        super(Company, self).delete(using, keep_parents)
        user.delete()
        return self
