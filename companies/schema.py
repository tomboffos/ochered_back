import graphene
from graphene_django import DjangoObjectType
from graphene_django.filter.utils import get_filtering_args_from_filterset
from graphene_django_optimizer import query as optimizer_query
from ochered.mutations import CustomSerializerMutation
from .models import Company
from .filters import CompanyFilter
from .serializers import CompanySerializer, CompanyDeleteSerializer


class CompanyType(DjangoObjectType):
    class Meta:
        model = Company


class CompanyQuery(graphene.ObjectType):
    company_list = graphene.List(CompanyType, **get_filtering_args_from_filterset(CompanyFilter, ''))
    company_detail = graphene.Field(CompanyType, id=graphene.ID(required=True))

    def resolve_company_list(self, info, **kwargs):
        queryset = Company.objects.perm_query(info.context.user)
        filtered = CompanyFilter(kwargs, queryset=queryset).qs
        return optimizer_query(filtered, info)

    def resolve_company_detail(self, info, **kwargs):
        queryset = Company.objects.perm_query(info.context.user)
        return optimizer_query(queryset, info).get(pk=kwargs['id'])


class CompanySave(CustomSerializerMutation):
    class Meta:
        serializer_class = CompanySerializer
        obj_type = CompanyType


class CompanyDelete(CustomSerializerMutation):
    class Meta:
        serializer_class = CompanyDeleteSerializer
        obj_type = CompanyType


class CompanyMutation(object):
    company_save = CompanySave.Field()
    company_delete = CompanyDelete.Field()
