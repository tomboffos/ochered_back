# Generated by Django 3.0.8 on 2020-08-09 09:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('categories', '0001_initial'),
        ('cities', '0001_initial'),
        ('companies', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='companies', to='categories.Category'),
        ),
        migrations.AlterField(
            model_name='company',
            name='city',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='cities.City'),
        ),
        migrations.AlterField(
            model_name='company',
            name='end',
            field=models.TimeField(null=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='name',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='company',
            name='start',
            field=models.TimeField(null=True),
        ),
    ]
