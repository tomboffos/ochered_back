from django.db.models import QuerySet


class CompanyQuerySet(QuerySet):
    def perm_query(self, user):
        if user.is_authenticated and (user.is_company or user.is_client or user.is_admin):
            return self

        return self

    def perm_save(self, user):
        if user.is_authenticated:
            return self.filter(user=user)

        return self.none()

    def perm_delete(self, user):
        if user.is_authenticated and user.is_admin:
            return self

        return None
