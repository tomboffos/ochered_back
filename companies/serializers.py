from rest_framework import serializers
from ochered.serializers import CustomModelSerializer
from users.serializers import UserSerializer
from .models import Company


class CompanySerializer(CustomModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Company
        exclude = ('scores_count', 'appointments_count')

    def create(self, validated_data):
        return Company.objects.create(**validated_data)


class CompanyDeleteSerializer(CustomModelSerializer):
    id = serializers.CharField()

    class Meta:
        model = Company
        fields = ('id',)

    def update(self, instance, validated_data):
        return instance.delete()
