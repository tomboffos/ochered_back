from django.template.defaultfilters import filesizeformat
from rest_framework import serializers
from .models import File
from .constants import CONTENT_TYPES, MAX_UPLOAD_SIZE


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = '__all__'
        read_only_fields = ('content_type', 'size', 'name', 'used_count')

    def validate_path(self, val):
        if val.content_type in CONTENT_TYPES:
            if val.size > MAX_UPLOAD_SIZE[val.content_type]:
                raise serializers.ValidationError(
                    'Please keep filesize under '
                    + filesizeformat(MAX_UPLOAD_SIZE[val.content_type])
                    + ' for ' + val.content_type + 's. Current filesize ' + filesizeformat(val.size)
                )
        else:
            raise serializers.ValidationError('File type is not supported')
        return val
