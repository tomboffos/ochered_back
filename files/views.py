from rest_framework.parsers import MultiPartParser
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin
from .models import File
from .serializers import FileSerializer


class FilesViewSet(CreateModelMixin, GenericViewSet):
    parser_classes = (MultiPartParser,)
    serializer_class = FileSerializer
    queryset = File.objects.all()
