# 2.5MB - 2621440
# 5MB - 5242880
# 10MB - 10485760
# 20MB - 20971520
# 50MB - 52428800
# 100MB 104857600
# 250MB - 214958080
# 500MB - 429916160

CONTENT_TYPES = ['image/jpg', 'image/jpeg', 'image/png']

MAX_UPLOAD_SIZE = {
    'image/jpg': 2621440,
    'image/jpeg': 2621440,
    'image/png': 2621440,
}
