from graphene_django import DjangoObjectType
from .models import File


class FileType(DjangoObjectType):
    class Meta:
        model = File

    def resolve_path(self, info):
        if str(self.path)[:4] == 'http':
            return self.path

        return 'http{}://{}{}{}'.format(
            's' if info.context.is_secure() else '',
            info.context.get_host(),
            '/media/',
            self.path,
        )
