import os
from django.db import models
from ochered.settings import MEDIA_ROOT
from ochered.models import TimestampedModel


class FileManager(models.Manager):
    def create(self, *args, **kwargs):
        kwargs['name'] = kwargs['path']
        return super(FileManager, self).create(
            *args,
            **kwargs,
            content_type=kwargs['path'].content_type,
            size=kwargs['path'].size,
        )


class File(TimestampedModel):
    path = models.FileField(upload_to='files/')
    content_type = models.CharField(max_length=30)
    size = models.FloatField()
    name = models.TextField()
    used_count = models.PositiveSmallIntegerField(default=0)

    objects = FileManager()

    def increase(self):
        self.used_count += 1
        self.save()

    def decrease(self):
        self.used_count -= 1
        self.save()

    def delete(self, using=None, keep_parents=False):
        os.remove(os.path.join(MEDIA_ROOT, str(self.path)))
        super(File, self).delete(using=using, keep_parents=keep_parents)
