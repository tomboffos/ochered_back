--
-- PostgreSQL database dump
--

-- Dumped from database version 10.14
-- Dumped by pg_dump version 10.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: appointments_appointment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appointments_appointment (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    start timestamp with time zone,
    client_id integer NOT NULL,
    master_id integer,
    service_id integer NOT NULL,
    status character varying(10) NOT NULL,
    comment_client text NOT NULL,
    comment_company text NOT NULL,
    score smallint NOT NULL,
    number smallint,
    finished_at timestamp with time zone,
    started_at timestamp with time zone,
    "end" timestamp with time zone,
    CONSTRAINT appointments_appointment_number_check CHECK ((number >= 0)),
    CONSTRAINT appointments_appointment_score_check CHECK ((score >= 0))
);


ALTER TABLE public.appointments_appointment OWNER TO postgres;

--
-- Name: appointments_appointment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.appointments_appointment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.appointments_appointment_id_seq OWNER TO postgres;

--
-- Name: appointments_appointment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.appointments_appointment_id_seq OWNED BY public.appointments_appointment.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO postgres;

--
-- Name: catalogs_catalog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.catalogs_catalog (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    name character varying(50) NOT NULL,
    image_id integer
);


ALTER TABLE public.catalogs_catalog OWNER TO postgres;

--
-- Name: catalogs_catalog_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.catalogs_catalog_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.catalogs_catalog_id_seq OWNER TO postgres;

--
-- Name: catalogs_catalog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.catalogs_catalog_id_seq OWNED BY public.catalogs_catalog.id;


--
-- Name: categories_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories_category (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    name character varying(100) NOT NULL,
    companies_count integer NOT NULL,
    catalog_id integer NOT NULL,
    cover_id integer,
    CONSTRAINT categories_category_companies_count_check CHECK ((companies_count >= 0))
);


ALTER TABLE public.categories_category OWNER TO postgres;

--
-- Name: categories_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categories_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_category_id_seq OWNER TO postgres;

--
-- Name: categories_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categories_category_id_seq OWNED BY public.categories_category.id;


--
-- Name: cities_city; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cities_city (
    id integer NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.cities_city OWNER TO postgres;

--
-- Name: cities_city_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cities_city_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cities_city_id_seq OWNER TO postgres;

--
-- Name: cities_city_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cities_city_id_seq OWNED BY public.cities_city.id;


--
-- Name: clients_client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clients_client (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    is_push boolean NOT NULL,
    city_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.clients_client OWNER TO postgres;

--
-- Name: clients_client_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clients_client_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clients_client_id_seq OWNER TO postgres;

--
-- Name: clients_client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clients_client_id_seq OWNED BY public.clients_client.id;


--
-- Name: companies_company; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.companies_company (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    name character varying(100) NOT NULL,
    start time without time zone,
    "end" time without time zone,
    category_id integer,
    city_id integer,
    logo_id integer,
    user_id integer NOT NULL,
    cover_id integer,
    appointments_count integer NOT NULL,
    scores_count integer NOT NULL,
    CONSTRAINT companies_company_appointments_count_check CHECK ((appointments_count >= 0)),
    CONSTRAINT companies_company_scores_count_check CHECK ((scores_count >= 0))
);


ALTER TABLE public.companies_company OWNER TO postgres;

--
-- Name: companies_company_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.companies_company_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.companies_company_id_seq OWNER TO postgres;

--
-- Name: companies_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.companies_company_id_seq OWNED BY public.companies_company.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: files_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.files_file (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    content_type character varying(30) NOT NULL,
    size double precision NOT NULL,
    name text NOT NULL,
    used_count smallint NOT NULL,
    CONSTRAINT files_file_used_count_check CHECK ((used_count >= 0))
);


ALTER TABLE public.files_file OWNER TO postgres;

--
-- Name: files_file_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.files_file_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.files_file_id_seq OWNER TO postgres;

--
-- Name: files_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.files_file_id_seq OWNED BY public.files_file.id;


--
-- Name: groups_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.groups_group (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    company_id integer NOT NULL
);


ALTER TABLE public.groups_group OWNER TO postgres;

--
-- Name: groups_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.groups_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_group_id_seq OWNER TO postgres;

--
-- Name: groups_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.groups_group_id_seq OWNED BY public.groups_group.id;


--
-- Name: likes_like; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.likes_like (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    client_id integer NOT NULL,
    company_id integer NOT NULL
);


ALTER TABLE public.likes_like OWNER TO postgres;

--
-- Name: likes_like_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.likes_like_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.likes_like_id_seq OWNER TO postgres;

--
-- Name: likes_like_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.likes_like_id_seq OWNED BY public.likes_like.id;


--
-- Name: masters_master; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.masters_master (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    specialty character varying(50) NOT NULL,
    fields character varying(200)[] NOT NULL,
    "values" character varying(200)[] NOT NULL,
    company_id integer NOT NULL,
    user_id integer NOT NULL,
    appointments_count integer NOT NULL,
    scores_count integer NOT NULL,
    total_count integer NOT NULL,
    CONSTRAINT masters_master_appointments_count_check CHECK ((appointments_count >= 0)),
    CONSTRAINT masters_master_scores_count_check CHECK ((scores_count >= 0)),
    CONSTRAINT masters_master_total_count_check CHECK ((total_count >= 0))
);


ALTER TABLE public.masters_master OWNER TO postgres;

--
-- Name: masters_master_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.masters_master_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.masters_master_id_seq OWNER TO postgres;

--
-- Name: masters_master_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.masters_master_id_seq OWNED BY public.masters_master.id;


--
-- Name: positions_position; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.positions_position (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    master_id integer NOT NULL,
    service_id integer NOT NULL
);


ALTER TABLE public.positions_position OWNER TO postgres;

--
-- Name: positions_position_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.positions_position_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.positions_position_id_seq OWNER TO postgres;

--
-- Name: positions_position_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.positions_position_id_seq OWNED BY public.positions_position.id;


--
-- Name: services_service; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.services_service (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    name character varying(100) NOT NULL,
    duration smallint NOT NULL,
    group_id integer NOT NULL,
    CONSTRAINT services_service_duration_check CHECK ((duration >= 0))
);


ALTER TABLE public.services_service OWNER TO postgres;

--
-- Name: services_service_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.services_service_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.services_service_id_seq OWNER TO postgres;

--
-- Name: services_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.services_service_id_seq OWNED BY public.services_service.id;


--
-- Name: users_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    created_at timestamp with time zone NOT NULL,
    username character varying(50) NOT NULL,
    is_active boolean NOT NULL,
    role character varying(10) NOT NULL,
    name character varying(100) NOT NULL,
    email character varying(254) NOT NULL,
    phone character varying(50) NOT NULL,
    avatar_id integer
);


ALTER TABLE public.users_user OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_id_seq OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users_user.id;


--
-- Name: appointments_appointment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointments_appointment ALTER COLUMN id SET DEFAULT nextval('public.appointments_appointment_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: catalogs_catalog id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogs_catalog ALTER COLUMN id SET DEFAULT nextval('public.catalogs_catalog_id_seq'::regclass);


--
-- Name: categories_category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_category ALTER COLUMN id SET DEFAULT nextval('public.categories_category_id_seq'::regclass);


--
-- Name: cities_city id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities_city ALTER COLUMN id SET DEFAULT nextval('public.cities_city_id_seq'::regclass);


--
-- Name: clients_client id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients_client ALTER COLUMN id SET DEFAULT nextval('public.clients_client_id_seq'::regclass);


--
-- Name: companies_company id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies_company ALTER COLUMN id SET DEFAULT nextval('public.companies_company_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: files_file id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.files_file ALTER COLUMN id SET DEFAULT nextval('public.files_file_id_seq'::regclass);


--
-- Name: groups_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.groups_group ALTER COLUMN id SET DEFAULT nextval('public.groups_group_id_seq'::regclass);


--
-- Name: likes_like id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.likes_like ALTER COLUMN id SET DEFAULT nextval('public.likes_like_id_seq'::regclass);


--
-- Name: masters_master id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.masters_master ALTER COLUMN id SET DEFAULT nextval('public.masters_master_id_seq'::regclass);


--
-- Name: positions_position id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.positions_position ALTER COLUMN id SET DEFAULT nextval('public.positions_position_id_seq'::regclass);


--
-- Name: services_service id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.services_service ALTER COLUMN id SET DEFAULT nextval('public.services_service_id_seq'::regclass);


--
-- Name: users_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user ALTER COLUMN id SET DEFAULT nextval('public.users_user_id_seq'::regclass);


--
-- Data for Name: appointments_appointment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.appointments_appointment (id, created_at, start, client_id, master_id, service_id, status, comment_client, comment_company, score, number, finished_at, started_at, "end") FROM stdin;
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add appointment	6	add_appointment
22	Can change appointment	6	change_appointment
23	Can delete appointment	6	delete_appointment
24	Can view appointment	6	view_appointment
25	Can add catalog	7	add_catalog
26	Can change catalog	7	change_catalog
27	Can delete catalog	7	delete_catalog
28	Can view catalog	7	view_catalog
29	Can add category	8	add_category
30	Can change category	8	change_category
31	Can delete category	8	delete_category
32	Can view category	8	view_category
33	Can add city	9	add_city
34	Can change city	9	change_city
35	Can delete city	9	delete_city
36	Can view city	9	view_city
37	Can add client	10	add_client
38	Can change client	10	change_client
39	Can delete client	10	delete_client
40	Can view client	10	view_client
41	Can add company	11	add_company
42	Can change company	11	change_company
43	Can delete company	11	delete_company
44	Can view company	11	view_company
45	Can add file	12	add_file
46	Can change file	12	change_file
47	Can delete file	12	delete_file
48	Can view file	12	view_file
49	Can add group	13	add_group
50	Can change group	13	change_group
51	Can delete group	13	delete_group
52	Can view group	13	view_group
53	Can add like	14	add_like
54	Can change like	14	change_like
55	Can delete like	14	delete_like
56	Can view like	14	view_like
57	Can add master	15	add_master
58	Can change master	15	change_master
59	Can delete master	15	delete_master
60	Can view master	15	view_master
61	Can add position	16	add_position
62	Can change position	16	change_position
63	Can delete position	16	delete_position
64	Can view position	16	view_position
65	Can add service	17	add_service
66	Can change service	17	change_service
67	Can delete service	17	delete_service
68	Can view service	17	view_service
69	Can add user	18	add_user
70	Can change user	18	change_user
71	Can delete user	18	delete_user
72	Can view user	18	view_user
73	Can add Token	19	add_token
74	Can change Token	19	change_token
75	Can delete Token	19	delete_token
76	Can view Token	19	view_token
77	Can add token	20	add_tokenproxy
78	Can change token	20	change_tokenproxy
79	Can delete token	20	delete_tokenproxy
80	Can view token	20	view_tokenproxy
\.


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
\.


--
-- Data for Name: catalogs_catalog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.catalogs_catalog (id, created_at, name, image_id) FROM stdin;
3	2021-04-01 20:43:01.999+06	Кафе	1
5	2021-04-02 06:59:13.745+06	Ресторан	3
6	2021-04-02 13:45:56.206+06	Бары	6
7	2021-04-02 13:48:03.94+06	Фаст фуд	5
\.


--
-- Data for Name: categories_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories_category (id, created_at, name, companies_count, catalog_id, cover_id) FROM stdin;
10	2021-04-01 20:53:46.458+06	Кафе	10	5	5
\.


--
-- Data for Name: cities_city; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cities_city (id, name) FROM stdin;
1	Алматы
\.


--
-- Data for Name: clients_client; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clients_client (id, created_at, is_push, city_id, user_id) FROM stdin;
2	2021-03-31 11:30:07.033384+06	t	1	2
3	2021-03-31 11:33:17.838508+06	t	1	3
4	2021-03-31 15:02:11.788775+06	t	1	4
5	2021-03-31 15:03:41.100738+06	t	1	5
6	2021-03-31 18:01:37.036363+06	t	1	6
7	2021-04-01 09:22:05.84192+06	t	1	7
8	2021-04-01 09:49:39.977756+06	t	1	8
9	2021-04-01 09:52:21.414117+06	t	1	9
10	2021-04-01 10:38:03.235106+06	t	1	10
11	2021-04-01 15:06:25.631461+06	t	1	11
12	2021-04-02 00:36:51.810638+06	t	1	12
\.


--
-- Data for Name: companies_company; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.companies_company (id, created_at, name, start, "end", category_id, city_id, logo_id, user_id, cover_id, appointments_count, scores_count) FROM stdin;
2	2021-04-02 14:23:48.965+06	Ресторан ААА\r\n	10:00:00	20:00:00	10	1	1	5	1	2	12
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	appointments	appointment
7	catalogs	catalog
8	categories	category
9	cities	city
10	clients	client
11	companies	company
12	files	file
13	groups	group
14	likes	like
15	masters	master
16	positions	position
17	services	service
18	users	user
19	authtoken	token
20	authtoken	tokenproxy
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	files	0001_initial	2021-03-28 18:18:29.77897+06
2	files	0002_auto_20200404_0308	2021-03-28 18:18:29.816977+06
3	files	0003_auto_20200406_1927	2021-03-28 18:18:30.00108+06
4	files	0004_auto_20200406_2220	2021-03-28 18:18:30.087944+06
5	users	0001_initial	2021-03-28 18:18:30.253897+06
6	contenttypes	0001_initial	2021-03-28 18:18:30.432801+06
7	admin	0001_initial	2021-03-28 18:18:30.498621+06
8	admin	0002_logentry_remove_auto_add	2021-03-28 18:18:30.561694+06
9	admin	0003_logentry_add_action_flag_choices	2021-03-28 18:18:30.565694+06
10	cities	0001_initial	2021-03-28 18:18:30.602225+06
11	catalogs	0001_initial	2021-03-28 18:18:30.639665+06
12	categories	0001_initial	2021-03-28 18:18:30.706204+06
13	companies	0001_initial	2021-03-28 18:18:30.847028+06
14	masters	0001_initial	2021-03-28 18:18:31.168716+06
15	masters	0002_remove_master_name	2021-03-28 18:18:31.228487+06
16	masters	0003_auto_20200811_0504	2021-03-28 18:18:31.489285+06
17	masters	0004_master_total_count	2021-03-28 18:18:31.702023+06
18	services	0001_initial	2021-03-28 18:18:31.769732+06
19	services	0002_auto_20200810_1649	2021-03-28 18:18:31.833797+06
20	positions	0001_initial	2021-03-28 18:18:31.884087+06
21	positions	0002_auto_20200811_1830	2021-03-28 18:18:31.96744+06
22	clients	0001_initial	2021-03-28 18:18:32.036392+06
23	appointments	0001_initial	2021-03-28 18:18:32.126221+06
24	appointments	0002_appointment_position	2021-03-28 18:18:32.175581+06
25	appointments	0003_auto_20200811_1853	2021-03-28 18:18:32.237516+06
26	appointments	0004_auto_20200811_1907	2021-03-28 18:18:32.44559+06
27	appointments	0005_auto_20200811_1908	2021-03-28 18:18:32.521632+06
28	appointments	0006_appointment_status	2021-03-28 18:18:32.64472+06
29	appointments	0007_auto_20200812_0254	2021-03-28 18:18:32.997215+06
30	appointments	0008_appointment_comment_user	2021-03-28 18:18:33.015134+06
31	appointments	0009_remove_appointment_comment_user	2021-03-28 18:18:33.063495+06
32	appointments	0010_appointment_score	2021-03-28 18:18:33.222064+06
33	appointments	0011_auto_20201119_1010	2021-03-28 18:18:33.265701+06
34	appointments	0012_auto_20201119_1810	2021-03-28 18:18:33.280713+06
35	appointments	0013_appointment_end	2021-03-28 18:18:33.290102+06
36	contenttypes	0002_remove_content_type_name	2021-03-28 18:18:33.306042+06
37	auth	0001_initial	2021-03-28 18:18:33.482633+06
38	auth	0002_alter_permission_name_max_length	2021-03-28 18:18:33.757897+06
39	auth	0003_alter_user_email_max_length	2021-03-28 18:18:33.764548+06
40	auth	0004_alter_user_username_opts	2021-03-28 18:18:33.770662+06
41	auth	0005_alter_user_last_login_null	2021-03-28 18:18:33.776786+06
42	auth	0006_require_contenttypes_0002	2021-03-28 18:18:33.779402+06
43	auth	0007_alter_validators_add_error_messages	2021-03-28 18:18:33.786484+06
44	auth	0008_alter_user_username_max_length	2021-03-28 18:18:33.792513+06
45	auth	0009_alter_user_last_name_max_length	2021-03-28 18:18:33.799785+06
46	auth	0010_alter_group_name_max_length	2021-03-28 18:18:33.814747+06
47	auth	0011_update_proxy_permissions	2021-03-28 18:18:33.828566+06
48	auth	0012_alter_user_first_name_max_length	2021-03-28 18:18:33.834438+06
49	catalogs	0002_auto_20200811_1609	2021-03-28 18:18:33.845414+06
50	clients	0002_auto_20200811_1547	2021-03-28 18:18:33.862501+06
51	clients	0003_auto_20201127_1435	2021-03-28 18:18:33.879503+06
52	clients	0004_auto_20210328_1817	2021-03-28 18:18:33.895591+06
53	companies	0002_auto_20200809_0927	2021-03-28 18:18:33.949134+06
54	companies	0003_auto_20200809_1343	2021-03-28 18:18:33.97799+06
55	companies	0004_auto_20200811_0504	2021-03-28 18:18:34.702146+06
56	groups	0001_initial	2021-03-28 18:18:34.776567+06
57	likes	0001_initial	2021-03-28 18:18:34.867362+06
58	positions	0003_auto_20201126_0421	2021-03-28 18:18:34.950838+06
59	services	0003_service_group	2021-03-28 18:18:35.075401+06
60	services	0004_auto_20201126_0713	2021-03-28 18:18:35.156064+06
61	services	0005_auto_20201126_2120	2021-03-28 18:18:35.179348+06
62	sessions	0001_initial	2021-03-28 18:18:35.300198+06
63	users	0002_auto_20200809_1248	2021-03-28 18:18:35.383136+06
64	users	0003_auto_20200811_0504	2021-03-28 18:18:35.393227+06
65	authtoken	0001_initial	2021-04-01 10:13:32.722638+06
66	authtoken	0002_auto_20160226_1747	2021-04-01 10:13:33.410173+06
67	authtoken	0003_tokenproxy	2021-04-01 10:13:33.415209+06
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- Data for Name: files_file; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.files_file (id, created_at, path, content_type, size, name, used_count) FROM stdin;
1	2021-04-01 20:40:59.384+06	http://10.0.2.2:8000/media/files/restoraunt.jpg	image	10	image	0
3	2021-04-02 13:42:58.422+06	http://10.0.2.2:8000/media/files/cafe.jpg	image	10	image	0
5	2021-04-02 13:46:32.412+06	http://10.0.2.2:8000/media/files/bar.png	image	10	image	0
6	2021-04-02 13:47:01.643+06	http://10.0.2.2:8000/media/files/burger.png	image	10	image	0
\.


--
-- Data for Name: groups_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.groups_group (id, name, company_id) FROM stdin;
\.


--
-- Data for Name: likes_like; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.likes_like (id, created_at, client_id, company_id) FROM stdin;
\.


--
-- Data for Name: masters_master; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.masters_master (id, created_at, specialty, fields, "values", company_id, user_id, appointments_count, scores_count, total_count) FROM stdin;
\.


--
-- Data for Name: positions_position; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.positions_position (id, created_at, master_id, service_id) FROM stdin;
\.


--
-- Data for Name: services_service; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.services_service (id, created_at, name, duration, group_id) FROM stdin;
\.


--
-- Data for Name: users_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_user (id, password, last_login, created_at, username, is_active, role, name, email, phone, avatar_id) FROM stdin;
2	pbkdf2_sha256$216000$1L7kw5auid5I$+tn8Y4Wcu6PpcMbOJXns1Nux4GIdxec0Ue06FQXDRso=	\N	2021-03-31 11:30:07.03038+06	87073039917s	t	CLIENT			87073039917s	\N
3	pbkdf2_sha256$216000$mPP0ZofKTyM7$ektx5YAMRQcz6YcPvWWJmnfona7zID2kImrB9uzspZ4=	\N	2021-03-31 11:33:17.43185+06	1010101010	t	CLIENT			1010101010	\N
4	pbkdf2_sha256$216000$0FdvT8V1J9WF$pAk1BNvVHKFEHe4aFE/vPx8ZwXj70/atsTO4BDmLjLc=	\N	2021-03-31 15:02:11.653862+06	87073039916	t	CLIENT			87073039916	\N
5	pbkdf2_sha256$216000$4BE074ILgFOg$jmmMtyUxN/ZNyKNJI2JkL6+fEk2ciu0xF5idoMu35QI=	\N	2021-03-31 15:03:40.957591+06	87073039919	t	CLIENT			87073039919	\N
6	pbkdf2_sha256$216000$s6a8QJ3DTJxc$hjnoy7k2wf6q8oqrZApadWKjBjv9Mvm6Pz+qlL/ua8E=	\N	2021-03-31 18:01:36.780038+06	87073039910	t	CLIENT			87073039910	\N
7	pbkdf2_sha256$216000$TOOLXYuvkahh$mvg/o3IIqkVEi+wbb3k+g6iGLynvt19HE/iTty1eICk=	\N	2021-04-01 09:22:05.838954+06	87073009999	t	CLIENT			87073009999	\N
8	pbkdf2_sha256$216000$THgre6bkU1X5$YD6JFBfq9eleVsTFRW93kGFTkGEusWN6U9hm1P8Ff74=	\N	2021-04-01 09:49:39.762612+06	87073039900	t	CLIENT			87073039900	\N
1	pbkdf2_sha256$216000$TOF4xXfmWDG2$HwmJ0QLpbjUv4j0V8AKKOLFNkc1rapwNeMT5BdxBqig=	\N	2021-03-31 11:28:33.25633+06	87073039917	t	CLIENT			870730399172	\N
9	pbkdf2_sha256$216000$claE2qwnM6f8$8KbHCAkiBxo2VrRovLDiFxpQQ1tWlC3sBzN2nMlRb+0=	\N	2021-04-01 09:52:21.412114+06	87073039917sssss	t	CLIENT			87073039917sssss	\N
10	pbkdf2_sha256$216000$UUR8zIQBqlsu$H8tnnRBtREfVEq+WhEB13oN2FeIUOS7IUAQaSAzaeyA=	\N	2021-04-01 10:38:03.232107+06	asy	t	CLIENT			asy	\N
11	pbkdf2_sha256$216000$hcSP3f23P3vP$Fx+yQ4G7ZNs5hMbPS4TfmP4LfcLZlz/wSWI8DzgCvZk=	\N	2021-04-01 15:06:25.530066+06	870730199127	t	CLIENT			870730199127	\N
12	pbkdf2_sha256$216000$NcVcVYrnvfJv$WpASdNIlkRiot9d55P7x6VfTTSdIUMt0dd9FhKrObGE=	\N	2021-04-02 00:36:51.521469+06	870730399172	t	CLIENT			870730399172	\N
\.


--
-- Name: appointments_appointment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.appointments_appointment_id_seq', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 80, true);


--
-- Name: catalogs_catalog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.catalogs_catalog_id_seq', 7, true);


--
-- Name: categories_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categories_category_id_seq', 10, true);


--
-- Name: cities_city_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cities_city_id_seq', 1, true);


--
-- Name: clients_client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clients_client_id_seq', 12, true);


--
-- Name: companies_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.companies_company_id_seq', 2, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 20, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 67, true);


--
-- Name: files_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.files_file_id_seq', 6, true);


--
-- Name: groups_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.groups_group_id_seq', 1, false);


--
-- Name: likes_like_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.likes_like_id_seq', 1, false);


--
-- Name: masters_master_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.masters_master_id_seq', 1, false);


--
-- Name: positions_position_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.positions_position_id_seq', 1, false);


--
-- Name: services_service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.services_service_id_seq', 1, false);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_user_id_seq', 12, true);


--
-- Name: appointments_appointment appointments_appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointments_appointment
    ADD CONSTRAINT appointments_appointment_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: catalogs_catalog catalogs_catalog_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogs_catalog
    ADD CONSTRAINT catalogs_catalog_pkey PRIMARY KEY (id);


--
-- Name: categories_category categories_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_category
    ADD CONSTRAINT categories_category_pkey PRIMARY KEY (id);


--
-- Name: cities_city cities_city_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities_city
    ADD CONSTRAINT cities_city_pkey PRIMARY KEY (id);


--
-- Name: clients_client clients_client_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients_client
    ADD CONSTRAINT clients_client_pkey PRIMARY KEY (id);


--
-- Name: clients_client clients_client_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients_client
    ADD CONSTRAINT clients_client_user_id_key UNIQUE (user_id);


--
-- Name: companies_company companies_company_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies_company
    ADD CONSTRAINT companies_company_pkey PRIMARY KEY (id);


--
-- Name: companies_company companies_company_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies_company
    ADD CONSTRAINT companies_company_user_id_key UNIQUE (user_id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: files_file files_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.files_file
    ADD CONSTRAINT files_file_pkey PRIMARY KEY (id);


--
-- Name: groups_group groups_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.groups_group
    ADD CONSTRAINT groups_group_pkey PRIMARY KEY (id);


--
-- Name: likes_like likes_like_client_id_company_id_8a47f13d_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.likes_like
    ADD CONSTRAINT likes_like_client_id_company_id_8a47f13d_uniq UNIQUE (client_id, company_id);


--
-- Name: likes_like likes_like_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.likes_like
    ADD CONSTRAINT likes_like_pkey PRIMARY KEY (id);


--
-- Name: masters_master masters_master_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.masters_master
    ADD CONSTRAINT masters_master_pkey PRIMARY KEY (id);


--
-- Name: masters_master masters_master_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.masters_master
    ADD CONSTRAINT masters_master_user_id_key UNIQUE (user_id);


--
-- Name: positions_position positions_position_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.positions_position
    ADD CONSTRAINT positions_position_pkey PRIMARY KEY (id);


--
-- Name: services_service services_service_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.services_service
    ADD CONSTRAINT services_service_pkey PRIMARY KEY (id);


--
-- Name: users_user users_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user
    ADD CONSTRAINT users_user_pkey PRIMARY KEY (id);


--
-- Name: users_user users_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user
    ADD CONSTRAINT users_user_username_key UNIQUE (username);


--
-- Name: appointments_appointment_client_id_c189cb9e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX appointments_appointment_client_id_c189cb9e ON public.appointments_appointment USING btree (client_id);


--
-- Name: appointments_appointment_master_id_4122d342; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX appointments_appointment_master_id_4122d342 ON public.appointments_appointment USING btree (master_id);


--
-- Name: appointments_appointment_service_id_945bc869; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX appointments_appointment_service_id_945bc869 ON public.appointments_appointment USING btree (service_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: catalogs_catalog_file_id_85c7b4da; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX catalogs_catalog_file_id_85c7b4da ON public.catalogs_catalog USING btree (image_id);


--
-- Name: categories_category_catalog_id_6336548f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX categories_category_catalog_id_6336548f ON public.categories_category USING btree (catalog_id);


--
-- Name: categories_category_cover_id_952b276e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX categories_category_cover_id_952b276e ON public.categories_category USING btree (cover_id);


--
-- Name: clients_client_city_id_298c997a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX clients_client_city_id_298c997a ON public.clients_client USING btree (city_id);


--
-- Name: companies_company_category_id_f0655418; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX companies_company_category_id_f0655418 ON public.companies_company USING btree (category_id);


--
-- Name: companies_company_city_id_124c3b36; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX companies_company_city_id_124c3b36 ON public.companies_company USING btree (city_id);


--
-- Name: companies_company_cover_id_14f0e4f9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX companies_company_cover_id_14f0e4f9 ON public.companies_company USING btree (cover_id);


--
-- Name: companies_company_logo_id_cccceb11; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX companies_company_logo_id_cccceb11 ON public.companies_company USING btree (logo_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: groups_group_company_id_b31688a1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX groups_group_company_id_b31688a1 ON public.groups_group USING btree (company_id);


--
-- Name: likes_like_client_id_45fdfb6a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX likes_like_client_id_45fdfb6a ON public.likes_like USING btree (client_id);


--
-- Name: likes_like_company_id_74bc5dc7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX likes_like_company_id_74bc5dc7 ON public.likes_like USING btree (company_id);


--
-- Name: masters_master_company_id_7e4d3911; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX masters_master_company_id_7e4d3911 ON public.masters_master USING btree (company_id);


--
-- Name: positions_position_master_id_a1f99e08; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX positions_position_master_id_a1f99e08 ON public.positions_position USING btree (master_id);


--
-- Name: positions_position_service_id_9c6dffc9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX positions_position_service_id_9c6dffc9 ON public.positions_position USING btree (service_id);


--
-- Name: services_service_group_id_899906bd; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX services_service_group_id_899906bd ON public.services_service USING btree (group_id);


--
-- Name: users_user_avatar_id_2054405b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_avatar_id_2054405b ON public.users_user USING btree (avatar_id);


--
-- Name: users_user_username_06e46fe6_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_user_username_06e46fe6_like ON public.users_user USING btree (username varchar_pattern_ops);


--
-- Name: appointments_appointment appointments_appoint_client_id_c189cb9e_fk_clients_c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointments_appointment
    ADD CONSTRAINT appointments_appoint_client_id_c189cb9e_fk_clients_c FOREIGN KEY (client_id) REFERENCES public.clients_client(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: appointments_appointment appointments_appoint_master_id_4122d342_fk_masters_m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointments_appointment
    ADD CONSTRAINT appointments_appoint_master_id_4122d342_fk_masters_m FOREIGN KEY (master_id) REFERENCES public.masters_master(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: appointments_appointment appointments_appoint_service_id_945bc869_fk_services_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointments_appointment
    ADD CONSTRAINT appointments_appoint_service_id_945bc869_fk_services_ FOREIGN KEY (service_id) REFERENCES public.services_service(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: catalogs_catalog catalogs_catalog_image_id_c1f45149_fk_files_file_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogs_catalog
    ADD CONSTRAINT catalogs_catalog_image_id_c1f45149_fk_files_file_id FOREIGN KEY (image_id) REFERENCES public.files_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: categories_category categories_category_catalog_id_6336548f_fk_catalogs_catalog_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_category
    ADD CONSTRAINT categories_category_catalog_id_6336548f_fk_catalogs_catalog_id FOREIGN KEY (catalog_id) REFERENCES public.catalogs_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: categories_category categories_category_cover_id_952b276e_fk_files_file_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories_category
    ADD CONSTRAINT categories_category_cover_id_952b276e_fk_files_file_id FOREIGN KEY (cover_id) REFERENCES public.files_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clients_client clients_client_city_id_298c997a_fk_cities_city_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients_client
    ADD CONSTRAINT clients_client_city_id_298c997a_fk_cities_city_id FOREIGN KEY (city_id) REFERENCES public.cities_city(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clients_client clients_client_user_id_57c8fc4a_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients_client
    ADD CONSTRAINT clients_client_user_id_57c8fc4a_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: companies_company companies_company_category_id_f0655418_fk_categorie; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies_company
    ADD CONSTRAINT companies_company_category_id_f0655418_fk_categorie FOREIGN KEY (category_id) REFERENCES public.categories_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: companies_company companies_company_city_id_124c3b36_fk_cities_city_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies_company
    ADD CONSTRAINT companies_company_city_id_124c3b36_fk_cities_city_id FOREIGN KEY (city_id) REFERENCES public.cities_city(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: companies_company companies_company_cover_id_14f0e4f9_fk_files_file_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies_company
    ADD CONSTRAINT companies_company_cover_id_14f0e4f9_fk_files_file_id FOREIGN KEY (cover_id) REFERENCES public.files_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: companies_company companies_company_logo_id_cccceb11_fk_files_file_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies_company
    ADD CONSTRAINT companies_company_logo_id_cccceb11_fk_files_file_id FOREIGN KEY (logo_id) REFERENCES public.files_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: companies_company companies_company_user_id_175c2d31_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies_company
    ADD CONSTRAINT companies_company_user_id_175c2d31_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: groups_group groups_group_company_id_b31688a1_fk_companies_company_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.groups_group
    ADD CONSTRAINT groups_group_company_id_b31688a1_fk_companies_company_id FOREIGN KEY (company_id) REFERENCES public.companies_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: likes_like likes_like_client_id_45fdfb6a_fk_clients_client_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.likes_like
    ADD CONSTRAINT likes_like_client_id_45fdfb6a_fk_clients_client_id FOREIGN KEY (client_id) REFERENCES public.clients_client(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: likes_like likes_like_company_id_74bc5dc7_fk_companies_company_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.likes_like
    ADD CONSTRAINT likes_like_company_id_74bc5dc7_fk_companies_company_id FOREIGN KEY (company_id) REFERENCES public.companies_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: masters_master masters_master_company_id_7e4d3911_fk_companies_company_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.masters_master
    ADD CONSTRAINT masters_master_company_id_7e4d3911_fk_companies_company_id FOREIGN KEY (company_id) REFERENCES public.companies_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: masters_master masters_master_user_id_ed89da66_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.masters_master
    ADD CONSTRAINT masters_master_user_id_ed89da66_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: positions_position positions_position_master_id_a1f99e08_fk_masters_master_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.positions_position
    ADD CONSTRAINT positions_position_master_id_a1f99e08_fk_masters_master_id FOREIGN KEY (master_id) REFERENCES public.masters_master(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: positions_position positions_position_service_id_9c6dffc9_fk_services_service_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.positions_position
    ADD CONSTRAINT positions_position_service_id_9c6dffc9_fk_services_service_id FOREIGN KEY (service_id) REFERENCES public.services_service(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: services_service services_service_group_id_899906bd_fk_groups_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.services_service
    ADD CONSTRAINT services_service_group_id_899906bd_fk_groups_group_id FOREIGN KEY (group_id) REFERENCES public.groups_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user users_user_avatar_id_2054405b_fk_files_file_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_user
    ADD CONSTRAINT users_user_avatar_id_2054405b_fk_files_file_id FOREIGN KEY (avatar_id) REFERENCES public.files_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

