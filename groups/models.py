from django.db import models
from ochered.models import BaseModel
from companies.models import Company
from .querysets import GroupQuerySet


class Group(BaseModel):
    company = models.ForeignKey(Company, related_name='groups', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    objects = GroupQuerySet.as_manager()
