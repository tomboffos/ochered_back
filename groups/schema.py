import graphene
from graphene_django import DjangoObjectType
from graphene_django.filter.utils import get_filtering_args_from_filterset
from ochered.mutations import CustomSerializerMutation
from graphene_django_optimizer import query as optimizer_query
from .models import Group
from .filters import GroupFilter
from .serializers import GroupSerializer


class GroupType(DjangoObjectType):
    is_created = graphene.Boolean()

    class Meta:
        model = Group


class GroupQuery(graphene.ObjectType):
    group_list = graphene.List(GroupType, **get_filtering_args_from_filterset(GroupFilter, ''))
    group_detail = graphene.Field(GroupType, id=graphene.ID(required=True))

    def resolve_group_list(self, info, **kwargs):
        queryset = Group.objects.perm_query(info.context.user)
        filtered = GroupFilter(kwargs, queryset=queryset).qs
        return optimizer_query(filtered, info)

    def resolve_group_detail(self, info, **kwargs):
        queryset = Group.objects.perm_query(info.context.user)
        return optimizer_query(queryset, info).get(pk=kwargs['id'])


class GroupSave(CustomSerializerMutation):
    class Meta:
        serializer_class = GroupSerializer
        obj_type = GroupType


class GroupMutation(object):
    group_save = GroupSave.Field()
