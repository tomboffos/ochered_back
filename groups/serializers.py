from ochered.serializers import CustomModelSerializer
from .models import Group


class GroupSerializer(CustomModelSerializer):
    class Meta:
        model = Group
        exclude = ('company',)

    def create(self, validated_data):
        instance = Group.objects.create(**validated_data, company=self.context['request'].user.company)
        instance.is_created = True
        return instance
