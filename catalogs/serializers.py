from rest_framework import serializers
from ochered.serializers import CustomModelSerializer
from .models import Catalog


class CatalogSerializer(CustomModelSerializer):
    class Meta:
        model = Catalog
        fields = '__all__'


class CatalogDeleteSerializer(CustomModelSerializer):
    id = serializers.CharField()

    class Meta:
        model = Catalog
        fields = ('id',)

    def validate(self, attrs):
        if self.instance.categories.exists():
            raise serializers.ValidationError('Categories not empty')
        return attrs

    def update(self, instance, validated_data):
        return instance.delete()
