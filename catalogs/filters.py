import django_filters
from .models import Catalog


class CatalogFilter(django_filters.FilterSet):
    class Meta:
        model = Catalog
        fields = {
        }
