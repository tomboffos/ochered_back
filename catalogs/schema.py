import graphene
from graphene_django import DjangoObjectType
from graphene_django.filter.utils import get_filtering_args_from_filterset
from graphene_django_optimizer import query as optimizer_query
from ochered.mutations import CustomSerializerMutation
from .serializers import CatalogSerializer, CatalogDeleteSerializer
from .models import Catalog
from .filters import CatalogFilter


class CatalogType(DjangoObjectType):
    class Meta:
        model = Catalog


class CatalogQuery(graphene.ObjectType):
    catalog_list = graphene.List(CatalogType, **get_filtering_args_from_filterset(CatalogFilter, ''))
    catalog_detail = graphene.Field(CatalogType, id=graphene.ID(required=True))

    def resolve_catalog_list(self, info, **kwargs):
        queryset = Catalog.objects.perm_query(info.context.user)
        filtered = CatalogFilter(kwargs, queryset=queryset).qs
        return optimizer_query(filtered, info)

    def resolve_catalog_detail(self, info, **kwargs):
        queryset = Catalog.objects.perm_query(info.context.user)
        return optimizer_query(queryset, info).get(pk=kwargs['id'])


class CatalogSave(CustomSerializerMutation):
    class Meta:
        serializer_class = CatalogSerializer
        obj_type = CatalogType


class CatalogDelete(CustomSerializerMutation):
    class Meta:
        serializer_class = CatalogDeleteSerializer
        obj_type = CatalogType


class CatalogMutation(object):
    catalog_save = CatalogSave.Field()
    catalog_delete = CatalogDelete.Field()
