from django.db import models
from ochered.models import TimestampedModel
from files.models import File
from .querysets import CatalogQuerySet


class Catalog(TimestampedModel):
    image = models.ForeignKey(File, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=50)

    objects = CatalogQuerySet.as_manager()
