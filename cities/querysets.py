from django.db.models import QuerySet


class CityQuerySet(QuerySet):
    def perm_query(self, user):
        if user.is_authenticated:
            return self

        return self.none()

    def perm_save(self, user):
        if user.is_authenticated and user.is_admin:
            return self

        return None

    def perm_delete(self, user):
        if user.is_authenticated and user.is_admin:
            return self

        return None
