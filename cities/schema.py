import graphene
from graphene_django import DjangoObjectType
from graphene_django.filter.utils import get_filtering_args_from_filterset
from ochered.mutations import CustomSerializerMutation
from graphene_django_optimizer import query as optimizer_query
from .models import City
from .filters import CityFilter
from .serializers import CitySerializer, CityDeleteSerializer


class CityType(DjangoObjectType):
    class Meta:
        model = City


class CityQuery(graphene.ObjectType):
    city_list = graphene.List(CityType, **get_filtering_args_from_filterset(CityFilter, ''))
    city_detail = graphene.Field(CityType, id=graphene.ID(required=True))

    def resolve_city_list(self, info, **kwargs):
        queryset = City.objects.perm_query(info.context.user)
        filtered = CityFilter(kwargs, queryset=queryset).qs
        return optimizer_query(filtered, info)

    def resolve_city_detail(self, info, **kwargs):
        queryset = City.objects.perm_query(info.context.user)
        return optimizer_query(queryset, info).get(pk=kwargs['id'])


class CitySave(CustomSerializerMutation):
    class Meta:
        serializer_class = CitySerializer
        obj_type = CityType


class CityDelete(CustomSerializerMutation):
    class Meta:
        serializer_class = CityDeleteSerializer
        obj_type = CityType


class CityMutation(object):
    city_save = CitySave.Field()
    city_delete = CityDelete.Field()
