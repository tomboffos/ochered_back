from rest_framework import serializers
from ochered.serializers import CustomModelSerializer
from .models import City


class CitySerializer(CustomModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class CityDeleteSerializer(CustomModelSerializer):
    id = serializers.CharField()

    class Meta:
        model = City
        fields = ('id',)

    def validate(self, attrs):
        if self.instance.id == 1:
            raise serializers.ValidationError('First city not deletable')
        return attrs

    def update(self, instance, validated_data):
        return instance.delete()
