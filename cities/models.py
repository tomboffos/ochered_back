from django.db import models
from ochered.models import BaseModel
from .querysets import CityQuerySet


class City(BaseModel):
    name = models.CharField(max_length=50)

    objects = CityQuerySet.as_manager()
