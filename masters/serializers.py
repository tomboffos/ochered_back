from ochered.serializers import CustomModelSerializer
from users.serializers import UserMasterSerializer
from .models import Master


class MasterSerializer(CustomModelSerializer):
    user = UserMasterSerializer()

    class Meta:
        model = Master
        exclude = ('company', 'scores_count', 'appointments_count', 'total_count')

    def create(self, validated_data):
        return Master.objects.create(**validated_data, company=self.context['request'].user.company)
