from django.db.models import QuerySet


class MasterQuerySet(QuerySet):
    def perm_query(self, user):
        if user.is_authenticated and user.is_company:
            return self.filter(company=user.company)

        return self.none()

    def perm_save(self, user):
        if user.is_authenticated and user.is_company:
            return self.filter(company=user.company)

        return None

    def perm_delete(self, user):
        if user.is_authenticated and user.is_company:
            return self.filter(company=user.company)

        return None
