import django_filters
from .models import Master


class MasterFilter(django_filters.FilterSet):
    class Meta:
        model = Master
        fields = {
            'company': ['exact'],
        }
