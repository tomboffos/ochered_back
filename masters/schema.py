import graphene
from django.db.models import Count, Sum, Q
from graphene_django import DjangoObjectType
from graphene_django.filter.utils import get_filtering_args_from_filterset
from ochered.mutations import CustomSerializerMutation
from graphene_django_optimizer import query as optimizer_query
from .models import Master
from .filters import MasterFilter
from .serializers import MasterSerializer


class MasterType(DjangoObjectType):
    app_all = graphene.Int()
    app_finished = graphene.Int()
    score = graphene.Float()

    class Meta:
        model = Master

    def resolve_score(self, info, **kwargs):
        return self.score_sum / self.score_count


class MasterQuery(graphene.ObjectType):
    master_list = graphene.List(MasterType, **get_filtering_args_from_filterset(MasterFilter, ''))
    master_detail = graphene.Field(MasterType, id=graphene.ID(required=True))

    def resolve_master_list(self, info, **kwargs):
        queryset = Master.objects.perm_query(info.context.user).annotate(
            app_all=Count('appointments'),
            app_finished=Count('appointments', filter=Q(appointments__status='FINISHED')),
            score_count=Count('appointments', filter=Q(appointments__score__gt=0)),
            score_sum=Sum('appointments__score', filter=Q(appointments__score__gt=0)),
        )
        filtered = MasterFilter(kwargs, queryset=queryset).qs
        return optimizer_query(filtered, info)

    def resolve_master_detail(self, info, **kwargs):
        queryset = Master.objects.perm_query(info.context.user)
        return optimizer_query(queryset, info).get(pk=kwargs['id'])


class MasterSave(CustomSerializerMutation):
    class Meta:
        serializer_class = MasterSerializer
        obj_type = MasterType


class MasterMutation(object):
    master_save = MasterSave.Field()
