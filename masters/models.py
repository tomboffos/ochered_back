from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.conf import settings
from ochered.models import TimestampedModel
from companies.models import Company
from users.models import User
from users.constants import ROLE_MASTER
from .querysets import MasterQuerySet


class MasterManager(models.Manager):
    def create(self, **kwargs):
        user_kwargs = kwargs.pop('user')

        user = User.objects.create(**user_kwargs, role=ROLE_MASTER, phone=user_kwargs['username'])

        master = super(MasterManager, self).create(**kwargs, user=user)

        # for service in master.company.services.all():
        #     service.positions.create(master=master)

        return master


class Master(TimestampedModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, related_name='masters', on_delete=models.CASCADE)
    specialty = models.CharField(max_length=50)
    fields = ArrayField(models.CharField(max_length=200), blank=True)
    values = ArrayField(models.CharField(max_length=200), blank=True)
    scores_count = models.PositiveIntegerField(default=0)
    appointments_count = models.PositiveIntegerField(default=0)
    total_count = models.PositiveIntegerField(default=0)

    objects = MasterManager.from_queryset(MasterQuerySet)()

    def update(self, **kwargs):
        user_kwargs = kwargs.pop('user')
        self.user.update(**user_kwargs)
        return super(Master, self).update(**kwargs)

    def delete(self, using=None, keep_parents=False):
        user = self.user
        super(Master, self).delete(using, keep_parents)
        user.delete()
        return self
