from rest_framework import serializers


class CustomModelSerializer(serializers.ModelSerializer):
    id = serializers.CharField(required=False)

    def update(self, instance, validated_data):
        return instance.update(**validated_data)


class CustomPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return self.queryset.perm_query(self.context['request'].user)
