import re
from collections import OrderedDict

from django.shortcuts import get_object_or_404

import graphene
from graphene.relay.mutation import ClientIDMutation
from graphene.types import Field, InputField
from graphene.types.mutation import MutationOptions
from graphene.types.objecttype import yank_fields_from_attrs

from graphene_django.types import ErrorType
from graphene_django.rest_framework.serializer_converter import convert_serializer_field


class SerializerMutationOptions(MutationOptions):
    lookup_field = None
    model_class = None
    model_operations = ["create", "update"]
    serializer_class = None
    obj_type = None


def fields_for_serializer(serializer, only_fields, exclude_fields, is_input=False):
    fields = OrderedDict()
    for name, field in serializer.fields.items():
        is_not_in_only = only_fields and name not in only_fields
        is_excluded = (
            name
            in exclude_fields  # or
            # name in already_created_fields
        ) or (
            field.write_only and not is_input  # don't show write_only fields in Query
        )

        if is_not_in_only or is_excluded:
            continue

        fields[name] = convert_serializer_field(field, is_input=is_input, convert_choices_to_enum=False)
    return fields


class CustomSerializerMutation(ClientIDMutation):
    class Meta:
        abstract = True

    errors = graphene.List(
        ErrorType, description="May contain more than one error for same field."
    )

    @classmethod
    def __init_subclass_with_meta__(
        cls,
        lookup_field=None,
        serializer_class=None,
        model_class=None,
        obj_type=None,
        model_operations=("create", "update"),
        only_fields=(),
        exclude_fields=(),
        **options
    ):

        if not serializer_class:
            raise Exception("serializer_class is required for the SerializerMutation")

        if "update" not in model_operations and "create" not in model_operations:
            raise Exception('model_operations must contain "create" and/or "update"')

        serializer = serializer_class()
        if model_class is None:
            serializer_meta = getattr(serializer_class, "Meta", None)
            if serializer_meta:
                model_class = getattr(serializer_meta, "model", None)

        if lookup_field is None and model_class:
            lookup_field = model_class._meta.pk.name

        input_fields = fields_for_serializer(
            serializer, only_fields, exclude_fields, is_input=True
        )
        # output_fields = fields_for_serializer(
        #     serializer, only_fields, exclude_fields, is_input=False
        # )
        output_fields = OrderedDict()
        if obj_type:
            output_fields['obj'] = graphene.Field(obj_type)

        _meta = SerializerMutationOptions(cls)
        _meta.lookup_field = lookup_field
        _meta.model_operations = model_operations
        _meta.serializer_class = serializer_class
        _meta.model_class = model_class
        _meta.fields = yank_fields_from_attrs(output_fields, _as=Field)

        input_fields = yank_fields_from_attrs(input_fields, _as=InputField)
        super(CustomSerializerMutation, cls).__init_subclass_with_meta__(
            _meta=_meta, input_fields=input_fields, **options
        )

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        model_class = cls._meta.model_class
        instance = None

        if model_class:
            lookup_field = cls._meta.lookup_field
            method_parts = re.findall('[A-Z][^A-Z]*', cls.__name__)
            perm_name = 'perm_' + '_'.join(method_parts[1:]).lower()
            queryset = getattr(model_class.objects, perm_name)(info.context.user)

            if queryset is None:
                return cls(errors=[
                    {
                        'field': 'non_field_errors',
                        'messages': ['Permission denied.']
                    }
                ])

            if lookup_field in input:
                instance = queryset.filter(**{lookup_field: input[lookup_field]}).first()

                if not instance:
                    return cls(errors=[
                        {
                            'field': 'non_field_errors',
                            'messages': ['Object not found.']
                        }
                    ])

        serializer = cls._meta.serializer_class(data=input, context={"request": info.context}, instance=instance)

        if serializer.is_valid():
            return cls.perform_mutate(serializer, info)
        else:
            errors = ErrorType.from_errors(serializer.errors)

            return cls(errors=errors)

    @classmethod
    def perform_mutate(cls, serializer, info):
        obj = serializer.save()

        return cls(errors=None, obj=obj)


class BaseModelMutationOptions(MutationOptions):
    model = None


class DeleteMutation(graphene.Mutation):
    deleted_id = graphene.String()
    errors = graphene.List(
        ErrorType, description="May contain more than one error for same field."
    )

    @classmethod
    def __init_subclass_with_meta__(cls, model=None, _meta=None, **options):
        if not _meta:
            _meta = BaseModelMutationOptions(cls)
        _meta.model = model

        super(DeleteMutation, cls).__init_subclass_with_meta__(model=model, _meta=_meta, **options)

    class Arguments:
        id = graphene.ID()

    @classmethod
    def mutate(cls, root, info, **kwargs):
        queryset = cls._meta.model.objects.perm_delete(info.context.user)

        if queryset is None:
            return cls(errors=[
                {
                    'field': 'non_field_errors',
                    'messages': ['Permission denied.']
                }
            ])

        obj = queryset.filter(pk=kwargs['id']).first()

        if not obj:
            return cls(errors=[
                {
                    'field': 'non_field_errors',
                    'messages': ['Object not found.']
                }
            ])

        return cls(errors=None, deleted_id=obj.delete())


