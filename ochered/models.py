from django.db import models
from django.utils import timezone


class BaseModel(models.Model):
    class Meta:
        abstract = True
        ordering = ['id']

    def update(self, **kwargs):
        self.mass_assign(**kwargs)
        self.save()
        return self

    def mass_assign(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def delete(self, using=None, keep_parents=False):
        deleted_id = self.id
        super(BaseModel, self).delete(using, keep_parents)
        self.id = deleted_id
        return self


class ArchivalModel(BaseModel):
    deleted_at = models.DateTimeField(null=True)

    class Meta(BaseModel.Meta):
        abstract = True

    def delete(self, using=None, keep_parents=False):
        self.deleted_at = timezone.now()
        self.save()
        return self.id


class TimestampedModel(BaseModel):
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta(BaseModel.Meta):
        abstract = True
        ordering = ['-created_at']
