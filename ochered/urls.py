from django.conf.urls.static import static
from django.conf import settings
from django.urls import include, path, re_path
from graphene_django.views import GraphQLView
from django.views.decorators.csrf import csrf_exempt
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from rest_framework.routers import DefaultRouter

import appointments
from files.views import FilesViewSet
from appointments.views import AppointmentView
router = DefaultRouter()
router.register(r'files', FilesViewSet)

urlpatterns = [
    path('graphql/', csrf_exempt(GraphQLView.as_view(graphiql=True))),
    # path('',include('appointments.urls')),
    re_path(r'^api/', include(router.urls)),
    re_path(r'^api-token-auth/', obtain_jwt_token),
    re_path(r'^api-token-refresh/', refresh_jwt_token),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
