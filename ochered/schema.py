import graphene
from appointments.schema import AppointmentQuery, AppointmentMutation
from catalogs.schema import CatalogQuery, CatalogMutation
from categories.schema import CategoryQuery, CategoryMutation
from cities.schema import CityQuery, CityMutation
from clients.schema import ClientQuery, ClientMutation
from companies.schema import CompanyQuery, CompanyMutation
from files.schema import FileType
from groups.schema import GroupQuery, GroupMutation
from likes.schema import LikeQuery, LikeMutation
from masters.schema import MasterQuery, MasterMutation
from positions.schema import PositionQuery, PositionMutation
from services.schema import ServiceQuery, ServiceMutation
from users.schema import UserQuery


class Query(
    AppointmentQuery,
    CatalogQuery,
    CategoryQuery,
    CityQuery,
    ClientQuery,
    CompanyQuery,
    GroupQuery,
    LikeQuery,
    MasterQuery,
    PositionQuery,
    ServiceQuery,
    UserQuery,
    graphene.ObjectType,
):
    pass


class Mutation(
    AppointmentMutation,
    CatalogMutation,
    CategoryMutation,
    CityMutation,
    ClientMutation,
    CompanyMutation,
    GroupMutation,
    LikeMutation,
    MasterMutation,
    PositionMutation,
    ServiceMutation,
    graphene.ObjectType,
):
    pass


types = [
    FileType
]

schema = graphene.Schema(query=Query, mutation=Mutation, types=types)
