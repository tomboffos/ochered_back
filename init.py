from cities.models import City
from catalogs.models import Catalog
from categories.models import Category


City.objects.create(name='Алматы')
City.objects.create(name='Астана')
City.objects.create(name='Шымкент')

c1 = Catalog.objects.create(name='Каталог №1')
c2 = Catalog.objects.create(name='Каталог №2')
c3 = Catalog.objects.create(name='Каталог №3')

c4 = Catalog.objects.create(name='Horeca')
c5 = Catalog.objects.create(name='ЦОН')
c6 = Catalog.objects.create(name='Банки')

Category.objects.create(catalog=c4, name='Рестораны')
Category.objects.create(catalog=c4, name='Бары')
Category.objects.create(catalog=c4, name='Кафе')
Category.objects.create(catalog=c4, name='Отель')
Category.objects.create(catalog=c4, name='Хостел')

Category.objects.create(catalog=c5, name='Ауэзовкий')
Category.objects.create(catalog=c5, name='Бостонлыкский')

Category.objects.create(catalog=c6, name='Банки 1-го уровня')
Category.objects.create(catalog=c6, name='Банки 2-го уровня')

Category.objects.create(catalog=c1, name='Категория №1')
Category.objects.create(catalog=c1, name='Категория №2')
Category.objects.create(catalog=c1, name='Категория №3')

Category.objects.create(catalog=c2, name='Категория №4')
Category.objects.create(catalog=c2, name='Категория №5')
Category.objects.create(catalog=c2, name='Категория №6')

Category.objects.create(catalog=c3, name='Категория №7')
Category.objects.create(catalog=c3, name='Категория №8')
Category.objects.create(catalog=c3, name='Категория №9')
