import datetime
from appointments.models import Appointment

for appointment in Appointment.objects.all():
    appointment.end = appointment.start + datetime.timedelta(hours=appointment.service.duration)
    appointment.save()
