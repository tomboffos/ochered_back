# Generated by Django 3.0.8 on 2020-08-05 08:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('files', '0004_auto_20200406_2220'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('username', models.CharField(max_length=50, unique=True)),
                ('is_active', models.BooleanField(default=False)),
                ('role', models.CharField(choices=[('ADMIN', 'Administrator'), ('COMPANY', 'Company'), ('CLIENT', 'Client'), ('CASHIER', 'Cashier'), ('EMPLOYEE', 'Employee')], max_length=10)),
                ('name', models.CharField(blank=True, max_length=100)),
                ('email', models.EmailField(blank=True, max_length=254)),
                ('phone', models.CharField(blank=True, max_length=50)),
                ('avatar', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='files.File')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
