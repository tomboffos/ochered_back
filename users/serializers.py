from rest_framework import serializers
from ochered.serializers import CustomModelSerializer
from .models import User


EXTRA_KWARGS = {
    'password': {
        'allow_blank': True
    }
}


class UserSerializer(CustomModelSerializer):
    username = serializers.CharField(max_length=50)
    old_password = serializers.CharField(max_length=50, required=False, allow_blank=True)

    class Meta:
        model = User
        exclude = ('is_active', 'role')
        extra_kwargs = EXTRA_KWARGS

    def validate_password(self, val):
        if not self.parent.instance and not val:
            raise serializers.ValidationError('This field may not be blank.')
        return val

    def validate_username(self, val):
        query = User.objects.filter(username=val.lower())
        if self.parent.instance:
            query = query.exclude(pk=self.parent.instance.user_id)
        if query.exists():
            raise serializers.ValidationError('User with this email already exists.')
        return val

    def validate(self, attrs):
        if self.parent.instance and attrs.get('password', None) and not self.context['request'].user.check_password(attrs.get('old_password', '')):
            raise serializers.ValidationError('Invalid password')
        return attrs


class UserMasterSerializer(UserSerializer):
    class Meta(UserSerializer.Meta):
        extra_kwargs = {
            **EXTRA_KWARGS,
            'name': {
                'allow_blank': False,
            }
        }
