import graphene
from graphene_django import DjangoObjectType
from graphene_django.rest_framework.mutation import SerializerMutation
from graphene_django_optimizer import query as optimizer_query
from .models import User
# from .serializers import CompanyUserSerializer


class UserType(DjangoObjectType):
    class Meta:
        model = User


class UserQuery(graphene.ObjectType):
    user = graphene.Field(UserType)

    def resolve_user(self, info, **kwargs):
        print(info.context.user)
        user = info.context.user
        if user.is_authenticated:
            return user
        # return user
        # return None
        return User.objects.get(pk=2)

# class UserSaveMutation(SerializerMutation):
#     user = graphene.Field(UserType)
#
#     class Meta:
#         serializer_class = CompanyUserSerializer

#     @classmethod
#     def perform_mutate(cls, serializer, info):
#         obj = serializer.save()
#         return cls(errors=None, user=obj)


# class UserMutation(object):
#     user_save = UserSaveMutation.Field()
