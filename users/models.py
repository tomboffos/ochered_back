from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.hashers import make_password
from ochered.models import TimestampedModel
from files.models import File
from .constants import ROLES, ROLE_COMPANY, ROLE_CLIENT, ROLE_MASTER, ROLE_ADMIN


class UserManager(models.Manager):
    def create(self, username, password, role, **kwargs):
        if password != '':
            password = make_password(password)

        return super(UserManager, self).create(
            username=username.lower(),
            password=password,
            role=role,
            **kwargs,
        )

    def get_by_natural_key(self, username):
        return self.get(**{self.model.USERNAME_FIELD: username})


class User(AbstractBaseUser, TimestampedModel):
    avatar = models.ForeignKey(File, null=True, on_delete=models.SET_NULL)
    username = models.CharField(max_length=50, unique=True)
    is_active = models.BooleanField(default=True)
    role = models.CharField(max_length=10, choices=ROLES)
    name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(blank=True)
    phone = models.CharField(max_length=50, blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'

    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)
        self.is_admin = self.role == ROLE_ADMIN
        self.is_company = self.role == ROLE_COMPANY
        self.is_master = self.role == ROLE_MASTER
        self.is_client = self.role == ROLE_CLIENT

    def update(self, username, **kwargs):
        if 'password' in kwargs and kwargs['password'] != '':
            kwargs['password'] = make_password(kwargs['password'])
        else:
            kwargs['password'] = self.password
        return super(User, self).update(username=username.lower(), **kwargs)
