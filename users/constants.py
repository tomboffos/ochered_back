ROLE_ADMIN = 'ADMIN'
ROLE_COMPANY = 'COMPANY'
ROLE_CLIENT = 'CLIENT'
ROLE_MASTER = 'MASTER'

ROLES = (
    (ROLE_ADMIN, 'Administrator'),
    (ROLE_COMPANY, 'Company'),
    (ROLE_CLIENT, 'Client'),
    (ROLE_MASTER, 'Master'),
)
